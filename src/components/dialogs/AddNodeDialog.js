import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

class AddNodeDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };

        this.handleSave = this.handleSave.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
    }

    handleValueChange(value) {
        this.setState({
            ...this.state,
            value: value
        });
    }

    handleSave() {
        const {value} = this.state;
        this.props.onSave(value)
    };

    render() {
        const {isShow} = this.props;
        const {value} = this.state;

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.props.hide}
            />,
            <FlatButton
                label="Save"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleSave}
                disabled={'' === value}
            />,
        ];

        return (
            <div>
                <Dialog
                    title="Add Node"
                    actions={actions}
                    modal={false}
                    open={isShow}
                    onRequestClose={this.props.hide}
                >
                    <div>
                        <TextField
                            hintText=""
                            floatingLabelText="Value"
                            onChange={e => this.handleValueChange(e.target.value)}
                        /><br/>
                    </div>
                </Dialog>
            </div>
        );
    }
}

export default AddNodeDialog;