import React, {Component} from 'react';
import './tree.css';

class TreeView extends Component {
    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
    }

    onSelect(item) {
        if (false === item.isDeleted) {
            this.props.onSelect(item);
        }
    }

    render() {
        const level = this.props.level || 0;
        const {data, selected, onSelect} = this.props;

        return (
            data.map(item => {
                let className = 'tree-item';
                if (selected === item.id) {
                    className += ' selected'
                }
                if (true === item.isDeleted) {
                    className += ' deleted';
                }

                return (
                    <div key={item.id} className="tree">
                        <p className={className} onClick={e => this.onSelect(item)}>
                            {item.value}
                        </p>
                        {!!item.children &&
                        <TreeView level={level + 1} data={item.children} onSelect={onSelect} selected={selected}/>
                        }
                    </div>
                );
            })
        )
    }
}

export default TreeView;
