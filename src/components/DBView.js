import React, {Component} from 'react';
import {connect} from 'react-redux';
import TreeView from "./TreeView";
import * as db from './../reducers/db';
import * as cache from './../reducers/cache';
import Paper from 'material-ui/Paper';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import {ArrowBackIcon, ReloadIcon} from "./Icons";

class DBView extends Component {
    componentDidMount() {
        this.props.loadDb();
    }

    handleSelect(node) {
        this.props.selectNode(node.id);
    }

    resetDB() {
        this.props.resetDB();
    }

    moveToCache() {
        const {selected} = this.props;
        if (null === selected) {
            return;
        }

        this.props.moveToCache(selected);
    }

    render() {
        const {data, selected} = this.props;

        return (
            <Paper className="paper" zDepth={2} rounded={false}>
                <Toolbar>
                    <ToolbarGroup>
                        <ToolbarTitle text="DB View"/>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <IconButton
                            tooltip="Move to cache"
                            disabled={null === selected}
                            onClick={this.moveToCache.bind(this)}
                        >
                            <ArrowBackIcon/>
                        </IconButton>
                        <IconButton tooltip="Reset DB to default" onClick={this.resetDB.bind(this)}>
                            <ReloadIcon/>
                        </IconButton>
                    </ToolbarGroup>
                </Toolbar>
                <TreeView data={data} onSelect={this.handleSelect.bind(this)} selected={selected}/>
            </Paper>
        );
    }
}

export default connect(
    state => ({
        data: state.db.data,
        selected: state.db.selected
    }), ({
        loadDb: db.loadDb,
        selectNode: db.selectNode,
        resetDB: db.resetDB,
        moveToCache: cache.loadFromDb
    })
)(DBView)