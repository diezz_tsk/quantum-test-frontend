import React, {Component} from 'react';
import {connect} from 'react-redux';
import TreeView from "./TreeView";
import * as cache from './../reducers/cache';
import * as db from './../reducers/db';
import Paper from 'material-ui/Paper';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import {AddIcon, ReloadIcon, ArrowForwardIcon, DeleteIcon, EditIcon} from "./Icons";
import AddNodeDialog from "./dialogs/AddNodeDialog";


class CacheView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAddDialogShow: false
        };

        this.showAddDialog = this.showAddDialog.bind(this);
        this.hideAddDialog = this.hideAddDialog.bind(this);
        this.onDialogSave = this.onDialogSave.bind(this);
    }

    componentDidMount() {
        this.props.loadCache();
    }

    handleSelect(node) {
        this.props.selectNode(node.id);
    }

    clearCache() {
        this.props.resetCache();
    }

    addOne() {
        this.showAddDialog();
    }

    showAddDialog() {
        this.props.showAddDialog();
    }

    hideAddDialog() {
        this.props.hideAddDialog();
    }

    hasChanges() {
        const isTouched = (model) => {
            if (true === model.isTouched) {
                return true;
            }
            if (model.children.length) {
                for (let i = 0; i < model.children.length; i++) {
                    let child = model.children[i];
                    if (true === isTouched(child)) {
                        return true;
                    }
                }
            }

            return false;
        };

        const {data} = this.props;
        for (let i = 0; i < data.length; i++) {
            let model = data[i];
            if (true === isTouched(model)) {
                return true;
            }
        }

        return false;
    }

    deleteNode() {
        const {selected} = this.props;
        this.props.deleteNode(selected);
    }

    saveChanges() {
        this.props.saveChanges();
    }

    onDialogSave(value) {
        const payload = {
            parentId: this.props.selected,
            value
        };

        this.props.addNewNode(payload);
    }


    render() {
        const {data, selected, isAddDialogShow} = this.props;

        return (
            <Paper className="paper" zDepth={2} rounded={false}>
                <Toolbar>
                    <ToolbarGroup>
                        <ToolbarTitle text="Cache View"/>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <IconButton
                            tooltip="Add child node"
                            disabled={null === selected}
                            onClick={this.addOne.bind(this)}
                        >
                            <AddIcon/>
                        </IconButton>
                        <IconButton
                            tooltip="Save changes"
                            disabled={false === this.hasChanges()}
                            onClick={this.saveChanges.bind(this)}
                        >
                            <ArrowForwardIcon/>
                        </IconButton>
                        <IconButton
                            tooltip="Delete node"
                            disabled={null === selected}
                            onClick={this.deleteNode.bind(this)}
                        >
                            <DeleteIcon/>
                        </IconButton>
                        <IconButton
                            tooltip="Reset cache"
                            onClick={this.clearCache.bind(this)}
                        >
                            <ReloadIcon/>
                        </IconButton>
                    </ToolbarGroup>
                </Toolbar>
                <TreeView data={data} onSelect={this.handleSelect.bind(this)} selected={selected}/>
                <AddNodeDialog
                    title="Add Node"
                    isShow={isAddDialogShow}
                    hide={this.hideAddDialog}
                    onSave={this.onDialogSave}
                />
            </Paper>
        );
    }
}

export default connect(
    state => ({
        data: state.cache.data,
        selected: state.cache.selected,
        isAddDialogShow: state.cache.isAddDialogShow,
    }), ({
        loadCache: cache.loadCache,
        selectNode: cache.selectNode,
        resetCache: cache.resetCache,
        saveChanges: cache.saveChanges,
        addNewNode: cache.addNewNode,
        showAddDialog: cache.showAddDialog,
        hideAddDialog: cache.hideAddDialog,
        deleteNode: cache.deleteNode,
        loadDb: db.loadDb
    })
)(CacheView)