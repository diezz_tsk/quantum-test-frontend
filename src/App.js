import React, {Component} from 'react';
import DBView from './components/DBView';
import './App.css';
import AppBar from 'material-ui/AppBar';
import CacheView from './components/CacheView';

class App extends Component {
    render() {

        return (
            <div className="App">
                <div><AppBar/></div>
                <div className="row">
                    <CacheView/>
                    <DBView/>
                </div>
            </div>
        );
    }
}

export default App;

