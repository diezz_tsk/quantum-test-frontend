import axios from 'axios';
import * as cache from './cache';

export const LOAD_DB_REQUEST = 'api.load.db.request';
export const LOAD_DB_SUCCESS = 'api.load.db.success';
export const LOAD_DB_FAIL = 'api.load.db.fail';

const SELECT_DB_NODE = 'db.view.select.node';

const RESET_DB_SUCCESS = 'api.db.reset.success';

const initState = {
    data: [],
    selected: null,
    isDbFetching: false
};

export default function db(state = initState, action) {
    switch (action.type) {
        case LOAD_DB_REQUEST: {
            return {
                ...state,
                isDbFetching: true
            };
        }
        case LOAD_DB_SUCCESS:
        case RESET_DB_SUCCESS: {
            return {
                ...state,
                isDbFetching: false,
                data: action.data,
                selected: null
            }
        }
        case SELECT_DB_NODE: {
            return {
                ...state,
                selected: action.nodeId
            }
        }
        default: {
            return state
        }
    }
}

export function loadDb() {
    return dispatch => {
        dispatch({type: LOAD_DB_REQUEST});
        axios
            .get('/db')
            .then(response => {
                dispatch({type: LOAD_DB_SUCCESS, data: response.data.data})
            }, error => {
                dispatch({type: LOAD_DB_FAIL, error})
            })
    }
}

export const resetDB = () => dispatch => {
    axios
        .post('/db/reset')
        .then(response => {
            dispatch({type: RESET_DB_SUCCESS, data: response.data.data});
            return axios.post('/cache/reset');
        })
        .then(response => {
            dispatch({type: cache.CLEAR_CACHE_SUCCESS, data: response.data.data});
        })
};

export function selectNode(nodeId) {
    return dispatch => {
        dispatch({type: SELECT_DB_NODE, nodeId});
    }
}