import axios from 'axios';
import * as db from './db';

const LOAD_CACHE_REQUEST = 'api.cache.load.request';
const LOAD_CACHE_SUCCESS = 'api.cache.load.success';
const LOAD_CACHE_FAIL = 'api.cache.load.fail';

const SELECT_CACHE_NODE = 'cache.view.select.node';

export const CLEAR_CACHE_SUCCESS = 'api.cache.reset.success';

const LOAD_FROM_DB_SUCCESS = 'api.cache.load.from.db.success';

const ADD_NEW_NODE_SUCCESS = 'api.cache.add.new.node.success';
const ADD_NEW_NODE_FAIL = 'api.cache.add.new.node.fail';

const SHOW_ADD_DIALOG = 'cache.view.add.dialog.show';
const HIDE_ADD_DIALOG = 'cache.view.add.dialog.hide';

const SAVE_CHANGES_SUCCESS = 'api.cache.save.changes.success';

const DELETE_NODE_SUCCESS = 'api.cache.delete.node.success';

const initState = {
    data: [],
    selected: null,
    isAddDialogShow: false
};

export default function cache(state = initState, action) {
    switch (action.type) {
        case LOAD_CACHE_SUCCESS:
        case LOAD_FROM_DB_SUCCESS: {
            return {
                ...state,
                data: action.data
            };
        }
        case CLEAR_CACHE_SUCCESS:
        case SAVE_CHANGES_SUCCESS:
        case DELETE_NODE_SUCCESS: {
            return {
                ...state,
                data: action.data,
                selected: null
            };
        }
        case ADD_NEW_NODE_SUCCESS: {
            return {
                ...state,
                data: action.data,
                isAddDialogShow: false
            };
        }
        case ADD_NEW_NODE_FAIL: {
            return {
                ...state,
                isAddDialogShow: true
            }
        }
        case SELECT_CACHE_NODE: {
            return {
                ...state,
                selected: action.nodeId
            }
        }
        case SHOW_ADD_DIALOG: {
            return {
                ...state,
                isAddDialogShow: true,
                isEditDialogShow: false,
            }
        }
        case HIDE_ADD_DIALOG: {
            return {
                ...state,
                isAddDialogShow: false
            }
        }
        default: {
            return state
        }
    }
}

export const loadCache = () => dispatch => {
    dispatch({type: LOAD_CACHE_REQUEST});
    axios
        .get('/cache')
        .then(response => {
            dispatch({type: LOAD_CACHE_SUCCESS, data: response.data.data})
        }, error => {
            dispatch({type: LOAD_CACHE_FAIL, error})
        });
};

export const resetCache = () => dispatch => {
    axios
        .post('/cache/reset')
        .then(response => {
            dispatch({type: CLEAR_CACHE_SUCCESS, data: response.data.data});
        })
};

export const addNewNode = (payload) => dispatch => {
    axios
        .put('/cache', payload)
        .then(response => {
            dispatch({type: ADD_NEW_NODE_SUCCESS, data: response.data.data});
        })
};

export const saveChanges = () => dispatch => {
    axios
        .post('/cache/save')
        .then(response => {
            dispatch({type: SAVE_CHANGES_SUCCESS, data: response.data.data});
            return axios.get('/db');
        })
        .then(response => {
            dispatch({type: db.LOAD_DB_SUCCESS, data: response.data.data})
        });
};

export const loadFromDb = (nodeId) => dispatch => {
    axios
        .post(`/cache/load/${nodeId}`)
        .then(response => {
            dispatch({type: LOAD_FROM_DB_SUCCESS, data: response.data.data});
        });
};

export const showAddDialog = () => dispatch => {
    dispatch({type: SHOW_ADD_DIALOG});
};

export const hideAddDialog = () => dispatch => {
    dispatch({type: HIDE_ADD_DIALOG});
};

export const selectNode = nodeId => dispatch => {
    dispatch({type: SELECT_CACHE_NODE, nodeId})
};

export const deleteNode = (nodeId) => dispatch => {
    axios
        .delete(`/cache/${nodeId}`)
        .then(response => {
            dispatch({type: DELETE_NODE_SUCCESS, data: response.data.data});
        });
};