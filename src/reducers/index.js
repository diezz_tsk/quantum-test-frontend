import {combineReducers} from "redux";
import db from './db';
import cache from './cache';
import axios from 'axios';

axios.defaults.baseURL = process.env.REACT_APP_API_BASE_URL;

export default combineReducers({
    db,
    cache
});
