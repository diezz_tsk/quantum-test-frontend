import {applyMiddleware, createStore} from "redux";
import reducers from "./../reducers";
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promiseMiddleware from "./../middleware/promiseMiddleware";

let middlewareList = [thunk, promiseMiddleware];
if ('development' === process.env.NODE_ENV) {
    middlewareList.push(logger);
}

export default function configureStore() {
    return createStore(reducers, applyMiddleware(...middlewareList));
}
